<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\UserController;


Route::get('/',[HomeController::class , 'index'])->name("site.home");
Route::prefix('site')->group(function () {
    Route::get('/login',[HomeController::class , 'loginForm'])->name('user.login');
    Route::get("/my-account",[UserController::class ,'profile'])->name("user.my-account");


});

?>

