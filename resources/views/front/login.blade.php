@extends('front.layouts.app')
@section('content')

<div class="maincontent-area">
<div class="zigzag-bottom"></div>
<div class="container">
    <div class="row">
        <div class="col-md-12">

        <form enctype="multipart/form-data" action="#" class="checkout" method="post" name="checkout">

<div id="customer_details" class="col2-set">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="woocommerce-billing-fields">
            <h3> تسجيل حساب جديد  </h3>
           

            <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                <label class="" for="billing_first_name"> الاسم   <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_first_name" name="billing_first_name" class="input-text ">
            </p>

            <p id="billing_last_name_field" class="form-row form-row-last validate-required">
                <label class="" for="billing_last_name">الكنية <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_last_name" name="billing_last_name" class="input-text ">
            </p>
            <div class="clear"></div>

           
             
  

            <div class="clear"></div>

            <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                <label class="" for="billing_email">البريد الالكتروني <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_email" name="billing_email" class="input-text ">
            </p>

            <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                <label class="" for="billing_phone">رقم الهاتف <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_phone" name="billing_phone" class="input-text ">
            </p>
            <div class="clear"></div>

            <p id="billing_password_field" class="form-row form-row-last validate-required validate-password">
                <label class="" for="billing_password">كلمة السر <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_password" name="billing_password" class="input-text ">
            </p>


            
            <p id="billing_confirm_password_field" class="form-row form-row-last validate-required validate-confirm_password">
                <label class="" for="billing_confirm_password"> تأكيد كلمة السر <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_confirm_password" name="billing_confirm_password" class="input-text ">
            </p>
            <div class="form-row place-order">

<input type="submit" data-value="Place order" value="تسجيل حساب" id="place_order" name="woocommerce_checkout_place_order" class="button alt">


</div>


        </div>
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="woocommerce-shipping-fields">
            <h3 id="ship-to-different-address">
<label class="checkbox" for="ship-to-different-address-checkbox"> لديك حساب ؟ تسجيل دخول</label>
<input type="checkbox" value="1" name="ship_to_different_address" checked="checked" class="input-checkbox" id="ship-to-different-address-checkbox">
</h3>
            <div class="shipping_address" style="display: block;">
          
               
                
            <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                <label class="" for="billing_email">البريد الالكتروني <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_email" name="billing_email" class="input-text ">
            </p>

            
            <div class="clear"></div>

            <p id="billing_password_field" class="form-row form-row-last validate-required validate-password">
                <label class="" for="billing_password">كلمة السر <abbr title="required" class="required">*</abbr>
                </label>
                <input type="text" value="" placeholder="" id="billing_password" name="billing_password" class="input-text ">
            </p>

                <div class="clear"></div>
            <p>
            <div class="form-row place-order">

<input type="submit" data-value="Place order" value=" دخول  " id="place_order" name="woocommerce_checkout_place_order" class="button alt">


</div>
            </p>      

            </div>





           


        </div>

    </div>

</div>
  
</form>
        </div>
    </div>
</div>
</div>
@endsection